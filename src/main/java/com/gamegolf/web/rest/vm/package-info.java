/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gamegolf.web.rest.vm;
