/**
 * Data Access Objects used by WebSocket services.
 */
package com.gamegolf.web.websocket.dto;
